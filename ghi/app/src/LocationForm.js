import React, { useEffect, useState } from 'react';

function LocationForm() {

    const [states, setStates] = useState([]);
    const [name, setName] = useState("");
    const [city, setCity] = useState("");
    const [roomCount, setRoomCount] = useState("");
    const [currentState, setCurrentState] = useState("");

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleRoomCount = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }

    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const handleState = (event) => {
        const value = event.target.value;
        setCurrentState(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = currentState;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            const newLocation = await response.json();
            console.log(newLocation);

            setName("");
            setCity("");
            setCurrentState("");
            setRoomCount("");
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setStates(data.states);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" className="form-control" name="name" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleRoomCount} value={roomCount} placeholder="Room count" required type="number" id="room_count" className="form-control" name="room_count" />
                        <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCityChange} value={city} placeholder="City" required type="text" id="city" className="form-control" name="city" />
                        <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleState} required id="state" className="form-select" name="state" value={currentState}>
                        <option value="">Choose a state</option>
                        {states.map(state => {
                            return (
                                <option key={state.abbreviation} value={state.abbreviation}>
                                    {state.name}
                                </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default LocationForm;
