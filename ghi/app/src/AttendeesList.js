
function AttendeesList(props){
    return (
    <table className="table table-striped table-secondary">
        <thead>
        <tr>
            <th>Name</th>
            <th>conference</th>
        </tr>
        </thead>

        <tbody>
        {props.attendees.map(attendee => {
            return (
            <tr key={attendee.href}>
                <td>{ attendee.name }</td>
                <td>{ attendee.conference }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
    );
}

export default AttendeesList;
