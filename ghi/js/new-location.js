
window.addEventListener("DOMContentLoaded", async () => {
    const stateURL = "http://localhost:8000/api/states/";

    try {
        const stateList = await fetch(stateURL);

        const selectTag = document.getElementById('state');
        if (stateList.ok){
            const data = await stateList.json();
            for (let state of data.states){
                const option = document.createElement("option");
                option.value = state.abbreviation;
                option.innerHTML = state.name;

                selectTag.appendChild(option);
            }
        }
    }
    catch {

    }
})


window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        console.log(json)
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});
