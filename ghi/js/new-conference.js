window.addEventListener("DOMContentLoaded", async () => {
    const locationURL = "http://localhost:8000/api/locations/";
    try {
        const locationList = await fetch(locationURL);

        const selectTag = document.getElementById('location');
        if (locationList.ok){
            const data = await locationList.json();
            for (let location of data.locations){
                const option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;

                selectTag.appendChild(option);
            }
        }
    }
    catch {

    }
})




window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        console.log(json)
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
