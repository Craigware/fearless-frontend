function createCard(name, description, pictureUrl, start, end, location){
    return `
        <div class="card shadow-lg mb-4">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <p class="card-text">${start}-${end}</p>
            </div>
        </div>
    `
}



function alert(message){
    var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
    let wrapper = document.createElement('div');
    wrapper.innerHTML = `<div class="alert alert-danger" role="alert">${message}</div>`;

    alertPlaceholder.append(wrapper)
}


window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);
        if (!response.ok){
            alert(`The response for ${url} failed.`);
        } else {
            let currentColumn = 0;
            const data = await response.json();
            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;

                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts);
                    const ends = new Date(details.conference.ends);
                    const location = details.conference.location.name;
                    const startFormatted = starts.toLocaleDateString();
                    const endsFormatted = ends.toLocaleDateString();

                    const html = createCard(name, description, pictureUrl, startFormatted, endsFormatted, location);
                    if (currentColumn > -1){
                        const column = document.querySelector(`.column${currentColumn}`);
                        column.innerHTML += html;
                    }

                    currentColumn += 1;
                    if (currentColumn === 3) {
                        currentColumn = 0;
                    }
                    console.log(currentColumn);
                } else {
                    alert(`The response for ${detailUrl} failed.`);
                }
            }
        }
    } catch (error) {
        alert(`Something went wrong!`)
    }
})
